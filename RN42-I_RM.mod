PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
XCVR_RN42-I/RM
$EndINDEX
$MODULE XCVR_RN42-I/RM
Po 0 0 0 15 00000000 00000000 ~~
Li XCVR_RN42-I/RM
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -5.21812 -17.4121 1.00156 1.00156 0 0.05 N V 21 "XCVR_RN42-I/RM"
T1 -4.58512 11.3901 1.00221 1.00221 0 0.05 N V 21 "VAL**"
DS -6.7 9.4 6.7 9.4 0.127 24
DS 6.7 9.4 6.7 -11.1 0.127 24
DS 6.7 -11.1 6.7 -16.4 0.127 24
DS 6.7 -16.4 -6.7 -16.4 0.127 24
DS -6.7 -16.4 -6.7 -11.1 0.127 24
DS -6.7 -11.1 -6.7 9.4 0.127 24
DS -6.7 -11.1 6.7 -11.1 0.127 21
DP 0 0 0 0 4 0.381 24
Dl -6.7206 -16.4
Dl 6.7 -16.4
Dl 6.7 -11.1341
Dl -6.7206 -11.1341
DP 0 0 0 0 4 0.381 24
Dl -6.70606 -16.4
Dl 6.7 -16.4
Dl 6.7 -11.1101
Dl -6.70606 -11.1101
DS -6.7 -7.3 -6.7 -16.4 0.127 21
DS -6.7 -16.4 6.7 -16.4 0.127 21
DS 6.7 -16.4 6.7 -7.3 0.127 21
DS -6.7 7.3 -6.7 9.4 0.127 21
DS -6.7 9.4 -4.8 9.4 0.127 21
DS 6.7 7.3 6.7 9.4 0.127 21
DS 6.7 9.4 4.8 9.4 0.127 21
DS -7.75 10.75 7.75 10.75 0.05 24
DS 7.75 10.75 7.75 -16.75 0.05 24
DS 7.75 -16.75 -7.75 -16.75 0.05 24
DS -7.75 -16.75 -7.75 10.75 0.05 24
DC -7.25 -7.5 -7.15 -7.5 0.2 21
$PAD
Sh "1" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 -6.6
$EndPAD
$PAD
Sh "2" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 -5.4
$EndPAD
$PAD
Sh "3" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 -4.2
$EndPAD
$PAD
Sh "4" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 -3
$EndPAD
$PAD
Sh "5" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 -1.8
$EndPAD
$PAD
Sh "6" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 -0.6
$EndPAD
$PAD
Sh "7" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 0.6
$EndPAD
$PAD
Sh "8" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 1.8
$EndPAD
$PAD
Sh "9" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 3
$EndPAD
$PAD
Sh "10" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 4.2
$EndPAD
$PAD
Sh "11" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 5.4
$EndPAD
$PAD
Sh "12" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.4 6.6
$EndPAD
$PAD
Sh "29" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.1 9.4
$EndPAD
$PAD
Sh "34" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.8 9.4
$EndPAD
$PAD
Sh "33" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.6 9.4
$EndPAD
$PAD
Sh "32" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.6 9.4
$EndPAD
$PAD
Sh "31" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.8 9.4
$EndPAD
$PAD
Sh "28" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.1 9.4
$EndPAD
$PAD
Sh "30" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.1 9.4
$EndPAD
$PAD
Sh "35" R 2 0.8 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.1 9.4
$EndPAD
$PAD
Sh "24" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 -6.6
$EndPAD
$PAD
Sh "23" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 -5.4
$EndPAD
$PAD
Sh "22" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 -4.2
$EndPAD
$PAD
Sh "21" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 -3
$EndPAD
$PAD
Sh "20" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 -1.8
$EndPAD
$PAD
Sh "19" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 -0.6
$EndPAD
$PAD
Sh "18" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 0.6
$EndPAD
$PAD
Sh "17" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 1.8
$EndPAD
$PAD
Sh "16" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 3
$EndPAD
$PAD
Sh "15" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 4.2
$EndPAD
$PAD
Sh "14" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 5.4
$EndPAD
$PAD
Sh "13" R 2 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.4 6.6
$EndPAD
$PAD
Sh "S1" R 1.8 1 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.6 8.7
$EndPAD
$PAD
Sh "S2" R 1.8 1 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.6 8.7
$EndPAD
$PAD
Sh "S4" R 1.8 1 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.6 -10.3
$EndPAD
$PAD
Sh "S3" R 1.8 1 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.6 -10.3
$EndPAD
$EndMODULE XCVR_RN42-I/RM
